import React from 'react';
import './App.css';
import {Principal, Header, View, Register} from './components/index'
import 'bootstrap/dist/css/bootstrap.min.css';
import {HashRouter} from "react-router-dom";
import {Route, Switch} from "react-router";

function App() {
    return (
        <HashRouter>
            <Header/>
            <Switch>
                <Route path="/restaurante/:idRestaurant" component={View}/>
                <Route path="/home" component={Principal}/>
                <Route path="/register" component={Register}/>
                <Route path="/" component={Principal}/>
            </Switch>
        </HashRouter>
    );
}

export default App;
