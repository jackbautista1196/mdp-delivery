import React, {Component} from 'react';
import ItemsCarousel from 'react-items-carousel';
import KeyboardArrowLeftIcon from '@material-ui/icons/KeyboardArrowLeft';
import KeyboardArrowRightIcon from '@material-ui/icons/KeyboardArrowRight';
import ArrowForwardIosIcon from '@material-ui/icons/ArrowForwardIos';
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Button from "@material-ui/core/Button";
import home_restaurants from '../Variables/home_restaurants.jsx';
import restaurant_destacados from '../Variables/home_restaurants_destacados';
import {Link} from "react-router-dom";
import FastfoodIcon from "@material-ui/icons/Fastfood";

class Index extends Component {

    state = {
        childrenRestaurantTop1: [],
        childrenRestaurantTop2: [],
        activeItemIndex1: 0,
        activeItemIndex2: 0
    };

    createItemsCarouselOutstanding = (data, i) => {
        return (
            <div key={i} style={{marginLeft: 0, marginRight: 0, height: 300,}}>
                <Link to={`/restaurante/${data.id}`} replace>
                    <img alt={data.nombre_restaurante}
                         src={require(`../../assets/img/img-carousel/slider_${i + 1}.jpg`)}
                         style={{width: '100%'}}
                         className="img-responsive"/>
                </Link>
            </div>
        );
    };

    createItemsCarouselHome = (data,idRestaurant, i) => {
        return (
            <div key={i} style={{background: '#eeeeee'}}>
                <div key={i} style={{
                    marginTop: '10px',
                    width: 'auto'
                }}>
                    <Link to={`/restaurante/${idRestaurant}`}><img alt=""
                                                      src={require(`../../assets/img/img-carousel/slider_${i + 1}.jpg`)}
                                                      style={{width: '100%'}}
                                                      className="img-responsive"/></Link>
                </div>
                <div style={{width: '100%', height: '60px'}}>
                    <div
                        style={{
                            width: '65%',
                            height: '40px',
                            position: 'absolute',
                            display: 'flex',
                            justifyContent: 'left',
                            alignItems: 'center',
                            padding: '0px 10px'
                        }}
                    ><span style={{margin: 0, display: 'flex', alignItems: 'center', fontSize: '13px'}}>
                    <FastfoodIcon/>{data.nombre_plato}</span></div>
                    <div className="footer-arrow-principal">
                        <p style={{
                            position: 'absolute', color: "white",
                            fontSize: '15px'
                        }}>S/. 25</p>
                    </div>
                </div>

            </div>
        );

    };

    changeActiveItem1 = (activeItemIndex) => {
        console.log("activeItemIndex => ", activeItemIndex);
        this.setState({activeItemIndex1: activeItemIndex})
    };
    changeActiveItem2 = (activeItemIndex) => this.setState({activeItemIndex2: activeItemIndex});

    render() {

        const {activeItemIndex1, activeItemIndex2} = this.state;
        console.log(activeItemIndex1);
        let outstanding = restaurant_destacados.home_restaurants_destacados.map((data, i) =>
            this.createItemsCarouselOutstanding(data, i)
        );

        let restaurants = home_restaurants.home_restaurants.map((data, i) => {
            let itemsCarousel = data.platos.map((params, j) =>
                this.createItemsCarouselHome(params,data.id, j)
            );
            return (
                <div key={i} className={'restaurant-1'}>
                    <Row style={{
                        marginRight: '0px',
                        marginLeft: '0px',
                        display: 'flex',
                        justifyContent: 'space-between',
                        alignItems: 'center'
                    }}>
                        <Col style={{textAlign: 'left'}}>{data.nombre}</Col>
                        <Col style={{
                            textAlign: 'right', display: 'flex',
                            justifyContent: 'flex-end',
                            alignItems: 'center'
                        }}>
                            <Button size="small" variant="outlined" color="secondary">
                                VER TODOS <ArrowForwardIosIcon style={{fontSize: '15px'}}/>
                            </Button>
                        </Col>
                    </Row>
                    <ItemsCarousel
                        // Placeholder configurations
                        disabledPlaceHolder
                        // Carousel configurations
                        numberOfCards={1.5}
                        gutter={5}
                        showSlither={true}
                        firstAndLastGutter={true}
                        freeScrolling={true}

                        // Active item configurations
                        requestToChangeActive={this.changeActiveItem2}
                        activeItemIndex={activeItemIndex2}
                        activePosition={'center'}

                        chevronWidth={30}
                        rightChevron={<KeyboardArrowRightIcon style={{color: "white"}}/>}
                        leftChevron={<KeyboardArrowLeftIcon style={{color: "white"}}/>}
                        outsideChevron={false}
                    >
                        {itemsCarousel}
                    </ItemsCarousel>
                </div>
            );
        });
        return (
            <div>
                <div style={{marginBottom: '10px'}}>
                    <ItemsCarousel
                        // Placeholder configurations
                        disabledPlaceHolder
                        // Carousel configurations
                        numberOfCards={1}
                        gutter={0}
                        showSlither={true}
                        firstAndLastGutter={true}
                        freeScrolling={true}

                        // Active item configurations
                        requestToChangeActive={this.changeActiveItem1}
                        activeItemIndex={activeItemIndex1}
                        activePosition={'center'}

                        chevronWidth={30}
                        rightChevron={<KeyboardArrowRightIcon style={{color: "white"}}/>}
                        leftChevron={<KeyboardArrowLeftIcon style={{color: "white"}}/>}
                        outsideChevron={false}
                    >
                        {outstanding}
                    </ItemsCarousel>
                </div>
                <div className={'restaurants'}>
                    {
                        restaurants
                    }
                </div>
            </div>

        );
    }
}

export default Index;