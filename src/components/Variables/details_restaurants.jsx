let details_restaurants = [
    {
        "id": 1000,
        "nombre": "Amore Restaurante",
        "descripcion": "DESCRIPCION DE ALGUNA PROMOCION U OFERTA QUE TENGA EL LOCATARIO, O CUALQUIER INFORMACION QUE DESEE MOSTRAR EN ESTA SECCIÓN",
        "estado": 1,
        "direccion": "Av. san luis 1369",
        "distrito": 15,
        "telefono": 45454545,
        "categorias": {id: [1]},
        "comida": {id: [1, 4]}
    },
    {
        "id": 1001,
        "nombre": "Las 3 nortes",
        "categoria": 2016,
        "descripcion": "DESCRIPCION DE ALGUNA PROMOCION U OFERTA QUE TENGA EL LOCATARIO, O CUALQUIER INFORMACION QUE DESEE MOSTRAR EN ESTA SECCIÓN",
        "estado": 1,
        "direccion": "Av. san luis 1369",
        "distrito": 15,
        "telefono": 45454545,
        "categorias": {id: [1, 3]},
        "comida": {id: [1, 2]}
    },
    {
        "id": 1002,
        "nombre": "Boa Amazónica",
        "categoria": 2016,
        "descripcion": "DESCRIPCION DE ALGUNA PROMOCION U OFERTA QUE TENGA EL LOCATARIO, O CUALQUIER INFORMACION QUE DESEE MOSTRAR EN ESTA SECCIÓN",
        "estado": 1,
        "direccion": "Av. san luis 1369",
        "distrito": 15,
        "telefono": 45454545,
        "categorias": {id: [1, 2]},
        "comida": {id: [2]}
    },
    {
        "id": 1003,
        "nombre": "La Picante",
        "categoria": 2016,
        "descripcion": "DESCRIPCION DE ALGUNA PROMOCION U OFERTA QUE TENGA EL LOCATARIO, O CUALQUIER INFORMACION QUE DESEE MOSTRAR EN ESTA SECCIÓN",
        "estado": 1,
        "direccion": "Av. san luis 1369",
        "distrito": 15,
        "telefono": 45454545,
        "categorias": {id: [2]},
        "comida": {id: [3]}
    },
    {
        "id": 1004,
        "nombre": "Pollos FOcus",
        "categoria": 2016,
        "descripcion": "PRUEBA DE DETALLES DE RESTAURANTE",
        "estado": 1,
        "direccion": "Av. san luis 1369",
        "distrito": 15,
        "telefono": 45454545,
        "categorias": {id: [1, 2, 4]},
        "comida": {id: [3, 4]}
    }
];

module.exports = {details_restaurants};