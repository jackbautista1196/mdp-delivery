let home_restaurants = [
    {
        "nombre": "Amore Restaurante",
        "id": 1000,
        "categoria": 2,
        "platos": [
            {
                "nombre_plato": "Pizza FAmiliar",
                "foto": "imagen.jpg",
                "precio": 50
            },
            {
                "nombre_plato": "Pizza personal",
                "foto": "imagen.jpg",
                "precio": 50
            },
            {
                "nombre_plato": "Pizza Duo",
                "foto": "imagen.jpg",
                "precio": 50
            },
            {
                "nombre_plato": "Pizza Solitaria",
                "foto": "imagen.jpg",
                "precio": 50
            },
            {
                "nombre_plato": "Pizza grupal",
                "foto": "imagen.jpg",
                "precio": 50
            }
        ]
    },
    {
        "nombre": "Las 3 nortes",
        "id": 1001,
        "categoria": 2,
        "platos": [
            {
                "nombre_plato": "Pizza Americana",
                "foto": "imagen.jpg",
                "precio": 50
            },
            {
                "nombre_plato": "Pizza Americana",
                "foto": "imagen.jpg",
                "precio": 50
            },
            {
                "nombre_plato": "Pizza Americana",
                "foto": "imagen.jpg",
                "precio": 50
            },
            {
                "nombre_plato": "Pizza Americana",
                "foto": "imagen.jpg",
                "precio": 50
            },
            {
                "nombre_plato": "Pizza Americana",
                "foto": "imagen.jpg",
                "precio": 50
            }
        ]
    },
    {
        "nombre": "Boa Amazónica",
        "id": 1002,
        "categoria": 2,
        "platos": [
            {
                "nombre_plato": "Pizza Americana",
                "foto": "imagen.jpg",
                "precio": 50
            },
            {
                "nombre_plato": "Pizza Americana",
                "foto": "imagen.jpg",
                "precio": 50
            },
            {
                "nombre_plato": "Pizza Americana",
                "foto": "imagen.jpg",
                "precio": 50
            },
            {
                "nombre_plato": "Pizza Americana",
                "foto": "imagen.jpg",
                "precio": 50
            },
            {
                "nombre_plato": "Pizza Americana",
                "foto": "imagen.jpg",
                "precio": 50
            }
        ]
    },
    {
        "nombre": "La Picante",
        "id": 1003,
        "categoria": 2,
        "platos": [
            {
                "nombre_plato": "Pizza Americana",
                "foto": "imagen.jpg",
                "precio": 50
            },
            {
                "nombre_plato": "Pizza Americana",
                "foto": "imagen.jpg",
                "precio": 50
            },
            {
                "nombre_plato": "Pizza Americana",
                "foto": "imagen.jpg",
                "precio": 50
            },
            {
                "nombre_plato": "Pizza Americana",
                "foto": "imagen.jpg",
                "precio": 50
            },
            {
                "nombre_plato": "Pizza Americana",
                "foto": "imagen.jpg",
                "precio": 50
            }
        ]
    },
    {
        "nombre": "Pollos Focus",
        "id": 1004,
        "categoria": 2,
        "platos": [
            {
                "nombre_plato": "Pizza Americana",
                "foto": "imagen.jpg",
                "precio": 50
            },
            {
                "nombre_plato": "Pizza Americana",
                "foto": "imagen.jpg",
                "precio": 50
            },
            {
                "nombre_plato": "Pizza Americana",
                "foto": "imagen.jpg",
                "precio": 50
            },
            {
                "nombre_plato": "Pizza Americana",
                "foto": "imagen.jpg",
                "precio": 50
            },
            {
                "nombre_plato": "Pizza Americana",
                "foto": "imagen.jpg",
                "precio": 50
            }
        ]
    }
];

module.exports = {home_restaurants};