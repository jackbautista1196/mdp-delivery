let categorias_restaurantes = [
    {
        "id": 1,
        "nombre": "FastFood"
    },
    {
        "id": 2,
        "nombre": "Pizzeria"
    },
    {
        "id": 3,
        "nombre": "Huarike"
    },
    {
        "id": 4,
        "nombre": "Dark kitchen"
    }
];

module.exports = {categorias_restaurantes};