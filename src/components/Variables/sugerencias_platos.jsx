let sugerencias = [
    {
        "categoria": 1,
        "platos": [
            {
                "nombre_plato": "Pizza Americana 1",
                "foto": "imagen.jpg",
                "precio": 50,
                "id": 1515
            },
            {
                "nombre_plato": "Pizza Americana 2",
                "foto": "imagen.jpg",
                "precio": 50,
                "id": 1515
            },
            {
                "nombre_plato": "Pizza Americana 3",
                "foto": "imagen.jpg",
                "precio": 50,
                "id": 1515
            },
            {
                "nombre_plato": "Pizza Americana 4",
                "foto": "imagen.jpg",
                "precio": 50,
                "id": 1515
            },
            {
                "nombre_plato": "Pizza Americana 5",
                "foto": "imagen.jpg",
                "precio": 50,
                "id": 1515
            }
        ]
    },
    {
        "categoria": 2,
        "platos": [
            {
                "nombre_plato": "Pizza Hawaiana 1",
                "foto": "imagen.jpg",
                "precio": 50,
                "id": 1515
            },
            {
                "nombre_plato": "Pizza Hawaiana 2",
                "foto": "imagen.jpg",
                "precio": 50,
                "id": 1515
            },
            {
                "nombre_plato": "Pizza Hawaiana 3",
                "foto": "imagen.jpg",
                "precio": 50,
                "id": 1515
            },
            {
                "nombre_plato": "Pizza Hawaiana 4",
                "foto": "imagen.jpg",
                "precio": 50,
                "id": 1515
            },
            {
                "nombre_plato": "Pizza Hawaiana 5",
                "foto": "imagen.jpg",
                "precio": 50,
                "id": 1515
            }
        ]
    },
    {
        "categoria": 3,
        "platos": [
            {
                "nombre_plato": "Pizza con Piña 1",
                "foto": "imagen.jpg",
                "precio": 50,
                "id": 1515
            },
            {
                "nombre_plato": "Pizza Piña 2",
                "foto": "imagen.jpg",
                "precio": 50,
                "id": 1515
            },
            {
                "nombre_plato": "Pizza Piña 3",
                "foto": "imagen.jpg",
                "precio": 50,
                "id": 1515
            },
            {
                "nombre_plato": "Pizza Piña 4",
                "foto": "imagen.jpg",
                "precio": 50,
                "id": 1515
            },
            {
                "nombre_plato": "Pizza con Piña 5",
                "foto": "imagen.jpg",
                "precio": 50,
                "id": 1515
            }
        ]
    },
    {
        "categoria": 4,
        "platos": [
            {
                "nombre_plato": "Pizza con Jamon 1",
                "foto": "imagen.jpg",
                "precio": 50,
                "id": 1515
            },
            {
                "nombre_plato": "Pizza con Jamon 2",
                "foto": "imagen.jpg",
                "precio": 50,
                "id": 1515
            },
            {
                "nombre_plato": "Pizza con Jamon 3",
                "foto": "imagen.jpg",
                "precio": 50,
                "id": 1515
            },
            {
                "nombre_plato": "Pizza con Jamon 4",
                "foto": "imagen.jpg",
                "precio": 50,
                "id": 1515
            },
            {
                "nombre_plato": "Pizza con Jamon 5",
                "foto": "imagen.jpg",
                "precio": 50,
                "id": 1515
            },
            {
                "nombre_plato": "Pizza con Jamon 6",
                "foto": "imagen.jpg",
                "precio": 50,
                "id": 1515
            }
        ]
    },
];

module.exports = {sugerencias};