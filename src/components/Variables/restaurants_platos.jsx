let restaurant_platos =  [
    {
        "id": 1000,
        "nombre_restaurante": "Amore Restaurante",
        "nombre_plato": "Pizza Americana",
        "foto": "imagen.jpg",
        "precio": 50
    },
    {
        "id": 1001,
        "nombre_restaurante": "Las 3 nortes",
        "nombre_plato": "Ceviche Clásico",
        "foto": "imagen.jpg",
        "precio": 60
    },
    {
        "id": 1002,
        "nombre_restaurante": "Boa Amazónica",
        "nombre_plato": "Juane familiar",
        "foto": "imagen.jpg",
        "precio": 70
    },
    {
        "id": 1003,
        "nombre_restaurante": "La Picante",
        "nombre_plato": "Pasta larga",
        "foto": "imagen.jpg",
        "precio": 80
    },
    {
        "id": 1004,
        "nombre_restaurante": "Pollos FOcus",
        "nombre_plato": "Pollo a la braza",
        "foto": "imagen.jpg",
        "precio": 90
    }
]

module.exports = {restaurant_platos};