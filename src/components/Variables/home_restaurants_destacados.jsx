let home_restaurants_destacados = [
    {
        "id": 1515,
        "nombre_restaurante": "Amore Restaurante",
        "nombre_plato": "Pizza Americana",
        "foto": "imagen.jpg",
        "precio": 50
    },
    {
        "id": 1516,
        "nombre_restaurante": "Las 3 nortes",
        "nombre_plato": "Ceviche Clásico",
        "foto": "imagen.jpg",
        "precio": 50
    },
    {
        "id": 1517,
        "nombre_restaurante": "Boa Amazónica",
        "nombre_plato": "Juane familiar",
        "foto": "imagen.jpg",
        "precio": 50
    },
    {
        "id": 1518,
        "nombre_restaurante": "La Picante",
        "nombre_plato": "Pasta larga",
        "foto": "imagen.jpg",
        "precio": 50
    },
    {
        "id": 1520,
        "nombre_restaurante": "Pollos FOcus",
        "nombre_plato": "Pollo a la braza",
        "foto": "imagen.jpg",
        "precio": 50
    }
];

module.exports = {home_restaurants_destacados};