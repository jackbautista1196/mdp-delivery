let restaurant_carta =
    [{
        "id": 1004,
        "carta": [
            {
                "id" : "1",
                "descripcion" : "pastas",
                "items": [
                    {
                        "id": "1",
                        "nombre": "Lasagna Vegetariana",
                        "descripcion": "Exquisita lasagna",
                        "presentacion": "medio kilo",
                        "precio": 25,
                        "stock": 10
                    },
                    {
                        "id": "3",
                        "nombre": "Lasagna de carne",
                        "descripcion": "Ragú de carnes, bechamel, mozzarella y parmesano.",
                        "presentacion": "medio kilo",
                        "precio": 25,
                        "stock": 10
                    },
                    {
                        "id": "4",
                        "nombre": "Lasagna de Alcachofa",
                        "descripcion": "",
                        "presentacion": "medio kilo",
                        "precio": 25,
                        "stock": 10
                    },
                    {
                        "id": "1",
                        "nombre": "Lasagna Vegetariana",
                        "descripcion": "",
                        "presentacion": "un kilo",
                        "precio": 50,
                        "stock": 10
                    },
                    {
                        "id": "3",
                        "nombre": "Lasagna de carne",
                        "descripcion": "Ragú de carnes, bechamel, mozzarella y parmesano.",
                        "presentacion": "un kilo",
                        "precio": 50,
                        "stock": 10
                    },
                    {
                        "id": "4",
                        "nombre": "Lasagna de Alcachofa",
                        "descripcion": "",
                        "presentacion": "un kilo",
                        "precio": 50,
                        "stock": 10
                    }

                ]
            },
            {
                "id" : "2",
                "descripcion" : "pizzas",
                "items":  [
                    {
                        "id": "4",
                        "nombre": "calzone clásico",
                        "descripcion": "Salsa de tomate, mozzarella fresca, jamón, tomillo y orégano",
                        "presentacion": "un kilo",
                        "precio": 36,
                        "stock": 10
                    },
                    {
                        "id": "4",
                        "nombre": "Americana",
                        "descripcion": "Salsa de tomate, mozzarella fresca, tomillo, jamón y orégano",
                        "presentacion": "un kilo",
                        "precio": 36,
                        "stock": 10
                    }
                ]
            },
            {
                "id" : "3",
                "descripcion" : "vino tinto",
                "items":  [
                    {
                        "id": "4",
                        "nombre": "calzone clásico",
                        "descripcion": "Salsa de tomate, mozzarella fresca, jamón, tomillo y orégano",
                        "presentacion": "un kilo",
                        "precio": 36,
                        "stock": 10
                    },
                    {
                        "id": "4",
                        "nombre": "Americana",
                        "descripcion": "Salsa de tomate, mozzarella fresca, tomillo, jamón y orégano",
                        "presentacion": "un kilo",
                        "precio": 36,
                        "stock": 10
                    }
                ]
            },
            {
                "id" : "4",
                "descripcion" : "vino blanco",
                "items":  [
                    {
                        "id": "4",
                        "nombre": "calzone clásico",
                        "descripcion": "Salsa de tomate, mozzarella fresca, jamón, tomillo y orégano",
                        "presentacion": "un kilo",
                        "precio": 36,
                        "stock": 10
                    },
                    {
                        "id": "4",
                        "nombre": "Americana",
                        "descripcion": "Salsa de tomate, mozzarella fresca, tomillo, jamón y orégano",
                        "presentacion": "un kilo",
                        "precio": 36,
                        "stock": 10
                    }
                ]
            },
            {
                "id" : "5",
                "descripcion" : "bebidas",
                "items":  [
                    {
                        "id": "4",
                        "nombre": "calzone clásico",
                        "descripcion": "Salsa de tomate, mozzarella fresca, jamón, tomillo y orégano",
                        "presentacion": "un kilo",
                        "precio": 36,
                        "stock": 10
                    },
                    {
                        "id": "4",
                        "nombre": "Americana",
                        "descripcion": "Salsa de tomate, mozzarella fresca, tomillo, jamón y orégano",
                        "presentacion": "un kilo",
                        "precio": 36,
                        "stock": 10
                    }
                ]
            }
        ]
    }];

module.exports = {restaurant_carta};
