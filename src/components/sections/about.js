import React, {Component} from 'react';
import Typography from "@material-ui/core/Typography";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Button from "@material-ui/core/Button";
import AddIcon from '@material-ui/icons/Add';

class About extends Component {
    render() {
        return (
            <div>
                <div style={{
                    background: 'url(' + require('../../assets/img/img-carousel/slider_3.jpg') + ') no-repeat fixed center',

                    backgroundSize: 'cover',
                    height: '301px',
                    width: '100%',
                    textAlign: 'center'
                }}>
                    <div className={'caja-texto-presentacion2'}>
                        <Typography variant="h4" style={{textAlign: 'center'}}>FOCUS IT</Typography>
                        <p style={{
                            textAlign: 'justify',
                            margin: 0
                        }}>
                            Focus IT es una empresa peruana de consultoría y desarrollo de aplicaciones móviles, fundada
                            en el año 2014, que promueve, diseña y desarrolla servicios y soluciones a medida de sus
                            clientes

                            SOMOS UNA EMPRESA DE INNOVACIÓN Y DESARROLLO TRANSFORMANDO LOS NEGOCIOS DESDE LOS PROCESOS
                            HASTA LA INTERACCIÓN CON LOS CLIENTES A TRAVÉS DE LAS SOLUCIONES TECNOLÓGICAS
                        </p><br/>
                    </div>

                    <div>
                        <Typography style={{
                            borderBottom: '2px solid #ffc107',
                            fontWeight: '600'
                        }}>TOP VENTAS</Typography>

                        <div style={{borderBottom: '1px solid #000000'}}>
                            <Row style={{margin: '0'}}>
                                <Col xs={8} style={{marginTop: '10px'}}>
                                    <Typography variant="h6" style={{textAlign: 'center', fontSize: '1.1rem'}}>1/2 pollo
                                        a la leña</Typography>
                                    <div className={'caja-texto-presentacion'}
                                         style={{margin: '5px', padding: '0', fontSize: '12px'}}><p>HOLA
                                        DESCRIPCION DEL PLATO A VENDER, PUEDEN IR LOS INGREDIENTES O CARACTERISTICAS
                                        ESPECIALES</p>
                                    </div>
                                </Col>
                                <Col xs={4} style={{marginTop: '10px'}}>
                                    <Row style={{
                                        display: 'flex',
                                        justifyContent: 'center'
                                    }}>
                                        <p>S/.15.00</p>
                                    </Row>
                                    <Row style={{
                                        display: 'flex',
                                        justifyContent: 'center'
                                    }}>
                                        <Button style={{
                                            maxWidth: '35px',
                                            minWidth: '35px', borderRadius: '50%'
                                        }} variant="contained" color="primary"><AddIcon></AddIcon></Button>
                                    </Row>
                                </Col>
                            </Row>
                        </div>
                        <div>
                            <Row style={{margin: '0'}}>
                                <Col xs={8} style={{marginTop: '10px'}}>
                                    <Typography variant="h6" style={{textAlign: 'center', fontSize: '1.1rem'}}>1/2 pollo
                                        a la leña</Typography>
                                    <div className={'caja-texto-presentacion'}
                                         style={{margin: '5px', padding: '0', fontSize: '12px'}}><p>DESCRIPCION DEL
                                        PLATO A VENDER, PUEDEN IR LOS INGREDIENTES O CARACTERISTICAS ESPECIALES</p>
                                    </div>
                                </Col>
                                <Col xs={4} style={{marginTop: '10px'}}>
                                    <Row style={{
                                        display: 'flex',
                                        justifyContent: 'center'
                                    }}>
                                        <p>S/.15.00</p>
                                    </Row>
                                    <Row style={{
                                        display: 'flex',
                                        justifyContent: 'center'
                                    }}>
                                        <Button style={{
                                            maxWidth: '35px',
                                            minWidth: '35px', borderRadius: '50%'
                                        }} variant="contained" color="primary"><AddIcon></AddIcon></Button>
                                    </Row>
                                </Col>
                            </Row>
                        </div>
                    </div>
                    <br/>
                    <div>
                        <Typography style={{
                            borderBottom: '2px solid #000000',
                            fontWeight: '600'
                        }}>TOP PLATOS</Typography>

                        <div style={{borderBottom: '1px solid #000000'}}>
                            <Row style={{margin: '0'}}>
                                <Col xs={8} style={{marginTop: '10px'}}>
                                    <Typography variant="h6" style={{textAlign: 'center', fontSize: '1.1rem'}}>1/2 pollo
                                        a la leña</Typography>
                                    <div className={'caja-texto-presentacion'}
                                         style={{margin: '5px', padding: '0', fontSize: '12px'}}><p>DESCRIPCION DEL
                                        PLATO A VENDER, PUEDEN IR LOS INGREDIENTES O CARACTERISTICAS ESPECIALES</p>
                                    </div>
                                </Col>
                                <Col xs={4} style={{marginTop: '10px'}}>
                                    <Row style={{
                                        display: 'flex',
                                        justifyContent: 'center'
                                    }}>
                                        <p>S/.15.00</p>
                                    </Row>
                                    <Row style={{
                                        display: 'flex',
                                        justifyContent: 'center'
                                    }}>
                                        <Button style={{
                                            maxWidth: '35px',
                                            minWidth: '35px', borderRadius: '50%'
                                        }} variant="contained" color="primary"><AddIcon></AddIcon></Button>
                                    </Row>
                                </Col>
                            </Row>
                        </div>
                        <div>
                            <Row style={{margin: '0'}}>
                                <Col xs={8} style={{marginTop: '10px'}}>
                                    <Typography variant="h6" style={{textAlign: 'center', fontSize: '1.1rem'}}>1/2 pollo
                                        a la leña</Typography>
                                    <div className={'caja-texto-presentacion'}
                                         style={{margin: '5px', padding: '0', fontSize: '12px'}}><p>DESCRIPCION DEL
                                        PLATO A VENDER, PUEDEN IR LOS INGREDIENTES O CARACTERISTICAS ESPECIALES</p>
                                    </div>
                                </Col>
                                <Col xs={4} style={{marginTop: '10px'}}>
                                    <Row style={{
                                        display: 'flex',
                                        justifyContent: 'center'
                                    }}>
                                        <p>S/.15.00</p>
                                    </Row>
                                    <Row style={{
                                        display: 'flex',
                                        justifyContent: 'center'
                                    }}>
                                        <Button style={{
                                            maxWidth: '35px',
                                            minWidth: '35px', borderRadius: '50%'
                                        }} variant="contained" color="primary"><AddIcon></AddIcon></Button>
                                    </Row>
                                </Col>
                            </Row>
                        </div>
                        <br/>

                    </div>
                </div>
            </div>
        );
    }
}

export default About;