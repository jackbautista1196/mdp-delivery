import React, {Component} from 'react';
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import FastfoodIcon from '@material-ui/icons/Fastfood';
import LocationOnIcon from '@material-ui/icons/LocationOn';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import {Link} from "react-router-dom";
import Button from "@material-ui/core/Button";
import ArrowForwardIosIcon from "@material-ui/icons/ArrowForwardIos";
import {Accordion, Card} from "react-bootstrap";
import Typography from "@material-ui/core/Typography";
import AddIcon from "@material-ui/icons/Add";
import details_restaurants from "../Variables/details_restaurants";
import details_restaurants_carta from "../Variables/details_restaurants_carta";
import tipo_comida from "../Variables/tipos_comidas";
import sugerencias from "../Variables/sugerencias_platos";
import categorias from "../Variables/restaurants_categorias";

let sugerenciasByCategorias = [];

class View extends Component {

    componentDidMount = async () => {
        await this.functionChargeInformation();
        await this.functionGetSugerenciasArray();
        await this.functionChargeScroll();
    };

    functionGetSugerenciasArray = () => {
        let data = this.state.data_restaurant;
        data &&
        data.categorias.id.map((categoria,index) => {
            sugerencias.sugerencias.map((sugerencia,index) =>{
               if (sugerencia.categoria == categoria){
                   sugerencia.platos.map((platos,index) => {
                       sugerenciasByCategorias.push(platos);
                   });
               }
            });
        });
    };

    functionChargeScroll = () => {
        window.onscroll = () => {
            const top = document.documentElement.scrollTop;
            const document_height = document.documentElement.scrollHeight;
            const window_height = window.innerHeight;
            const html = this.state.html;
            let newStart = parseInt(html.length) * 2;
            let end = (parseInt(html.length) * 2) + 2;

            if (parseInt(top) === (parseInt(document_height) - parseInt(window_height))) {
                html.push(this.createChildren(newStart, end));
                this.setState({html: html});
            }
        };
    };

    functionChargeInformation = async () => {
        let idRestaurant = this.props.match.params.idRestaurant;
        const details_restaurant = details_restaurants.details_restaurants.find(detail => detail.id == idRestaurant);
        const cart_restaurant = details_restaurants_carta.restaurant_carta.find(detail_cart => detail_cart.id == idRestaurant);
        await this.setState({data_restaurant: details_restaurant, cart_restaurant: cart_restaurant});
    };

    createChildren = (start, end) =>
        //sugerencias.sugerencias[0].platos.map((data, i) =>
        sugerenciasByCategorias.map((data, i) =>
            (i >= start) &&
            (i < end) &&
            <div key={i} style={{
                margin: '10px',
                display: 'flex',
                justifyContent: 'center'
            }}>
                <div key={i} style={{
                    marginTop: '10px',
                    width: 'auto'
                }}>
                    <Link to="/prueba"><img alt={""} src={require(`../../assets/img/img-carousel/slider_1.jpg`)}
                                            style={{width: '100%'}}
                                            className="img-responsive"/></Link>
                    <div style={{width: '100%', height: '60px'}}>
                        <div
                            style={{
                                height: '40px',
                                position: 'absolute',
                                display: 'flex',
                                justifyContent: 'left',
                                alignItems: 'center',
                                padding: '0px 10px'
                            }}>
                    <span style={{margin: 0, display: 'flex', alignItems: 'center', fontSize: '13px'}}>
                        <FastfoodIcon/>{data.nombre_plato}
                    </span>
                        </div>
                        <div className="footer-arrow-principal">
                            <p style={{
                                position: 'absolute', color: "white",
                                fontSize: '15px'
                            }}>S/. 25</p>
                        </div>
                    </div>
                </div>
            </div>)
    ;

    addItem = (item) => {
        alert("Plato agregado: " + item.descripcion);
    };

    state = {
        childrenRestaurantTop1: [],
        activeItemIndex1: 0,
        html: []
    };

    render() {
        const tipo_comidas = [];
        const categorias_restaurant = [];
        const {html, data_restaurant, cart_restaurant} = this.state;
        console.log("html =>", data_restaurant);

        const tags = html.map(function (data, index) {
            return (
                <div key={index} style={{gridTemplateColumns: 'auto auto', flexWrap: 'wrap', display: 'grid'}}>
                    {data}
                </div>
            )
        });
        data_restaurant &&
        data_restaurant.comida.id.map(data => {
            let comida = tipo_comida.comidas.filter(comida => comida.id == data);
            tipo_comidas.push(comida[0]);
        });

        data_restaurant &&
        data_restaurant.categorias.id.map(data => {
            let categoria = categorias.categorias_restaurantes.filter(categoria => categoria.id == data);
            categorias_restaurant.push(categoria[0]);
        });

        return (
            <div>
                <div style={{
                    position: 'relative'
                }}>
                    <div style={{margin: '0px'}}>
                        <img alt="" src={require(`../../assets/img/img-carousel/slider_5.jpg`)}
                             style={{width: '100%', height: '300px'}}
                             className="img-responsive"/>
                    </div>
                    <div className={'caja-texto-presentacion'}>
                        <p style={{textAlign: 'justify', margin: 0}}>
                            {
                                data_restaurant &&
                                data_restaurant.descripcion
                            }
                        </p><br/>
                        <Accordion defaultActiveKey="1">
                            <Card>
                                <Card.Header style={{
                                    justifyContent: 'space-between',
                                    display: 'flex'
                                }}>
                                    <Accordion.Toggle as={Button} style={{
                                        width: '100%',
                                        display: 'flex',
                                        justifyContent: 'space-between'
                                    }} eventKey="0">
                                        Carta<ExpandMoreIcon/>
                                    </Accordion.Toggle>

                                </Card.Header>
                                <Accordion.Collapse key={"1"} eventKey="0">
                                    <Card.Body>
                                        {
                                            cart_restaurant &&
                                            cart_restaurant.carta.map(data =>
                                                <div key={data.id}>
                                                    <Typography key={data.id} style={{
                                                        borderBottom: '2px solid #ffc107',
                                                        fontWeight: '600',
                                                        textAlign: 'center'
                                                    }}>{data.descripcion}</Typography>

                                                    {
                                                        data.items.map(item =>
                                                            <div key={item.id} style={{borderTop: '1px solid #000000'}}>
                                                                <Row style={{margin: '0'}}>
                                                                    <Col xs={8} style={{marginTop: '10px'}}>
                                                                        <Typography variant="h6" style={{
                                                                            textAlign: 'center',
                                                                            fontSize: '1.1rem'
                                                                        }}>{item.nombre}</Typography>
                                                                        <div className={'caja-texto-presentacion'}
                                                                             style={{
                                                                                 margin: '5px',
                                                                                 padding: '0',
                                                                                 fontSize: '12px'
                                                                             }}>
                                                                            <p>{item.descripcion ? item.descripcion : 'Falta agregar descripción'}</p>
                                                                        </div>
                                                                    </Col>
                                                                    <Col xs={4} style={{marginTop: '10px'}}>
                                                                        <Row style={{
                                                                            display: 'flex',
                                                                            justifyContent: 'center'
                                                                        }}>
                                                                            <p>S/. {item.precio}</p>
                                                                        </Row>
                                                                        <Row style={{
                                                                            display: 'flex',
                                                                            justifyContent: 'center'
                                                                        }}>
                                                                            <Button style={{
                                                                                maxWidth: '35px',
                                                                                minWidth: '35px', borderRadius: '50%'
                                                                            }} variant="contained"
                                                                                    onClick={() => this.addItem(item)}
                                                                                    color="primary"><AddIcon/></Button>
                                                                        </Row>
                                                                    </Col>
                                                                </Row>
                                                            </div>
                                                        )
                                                    }
                                                </div>
                                            )
                                        }
                                    </Card.Body>
                                </Accordion.Collapse>
                            </Card>
                        </Accordion>
                        <br/>
                        <p style={{textAlign: 'left'}}><LocationOnIcon/>{data_restaurant && data_restaurant.direccion}
                        </p>
                        <p style={{textAlign: 'left'}}><LocationOnIcon/>{data_restaurant && data_restaurant.telefono}
                        </p>
                        <p style={{textAlign: 'left'}}><LocationOnIcon/>Tipos de Comida</p>
                        <ul>
                            {
                                tipo_comidas &&
                                tipo_comidas.map(comida =>
                                    <li key={comida.id}>{comida.nombre}</li>
                                )
                            }
                        </ul>
                        <p style={{textAlign: 'left'}}><LocationOnIcon/>Categorías del Restaurant</p>
                        <ul>
                            {
                                categorias_restaurant &&
                                categorias_restaurant.map(categoria =>
                                    <li key={categoria.id}>{categoria.nombre}</li>
                                )
                            }
                        </ul>
                    </div>
                    <div className={'restaurants'}>
                        <div className={'restaurant-1'}>
                            <Row style={{
                                marginRight: '0px',
                                marginLeft: '0px',
                                marginBottom: '10px',
                                display: 'flex',
                                justifyContent: 'space-between',
                                alignItems: 'center'
                            }}>
                                <Col style={{textAlign: 'left'}}>Beneficios Similares</Col>
                                <Col style={{
                                    textAlign: 'right', display: 'flex',
                                    justifyContent: 'flex-end',
                                    alignItems: 'center'
                                }}>
                                    <Button size="small" variant="outlined" color="secondary">
                                        VER TODOS <ArrowForwardIosIcon style={{fontSize: '15px'}}></ArrowForwardIosIcon>
                                    </Button>
                                </Col>
                            </Row>
                        </div>
                    </div>
                    {
                        tags
                    }
                </div>
            </div>
        );
    }
}

export default View;