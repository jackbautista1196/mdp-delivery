import React, {Component} from 'react';
import ExpansionPanelSummary from "@material-ui/core/ExpansionPanelSummary";
import Typography from "@material-ui/core/Typography";
import ExpansionPanelDetails from "@material-ui/core/ExpansionPanelDetails";
import ExpansionPanel from "@material-ui/core/ExpansionPanel";
import PageviewIcon from '@material-ui/icons/Pageview';
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import CircularProgress from '@material-ui/core/CircularProgress';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import MenuItem from "@material-ui/core/MenuItem";
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import HighlightOffIcon from '@material-ui/icons/HighlightOff';
import Avatar from '@material-ui/core/Avatar';


const currencies = [
    {
        value: '1',
        label: 'Restaurant 1',
    },
    {
        value: '2',
        label: 'Restaurant 2',
    },
    {
        value: '3',
        label: 'Restaurant 3',
    },
    {
        value: '4',
        label: 'Restaurant 4',
    },
];

class Header extends Component {

    state = {
        currency: '4',
        show: false,
        class: 'show-detail'
    };

    handleChange = () => console.log("hola");

    example = async () => {
        await this.setState({show: true});
        console.log("state =>", this.state)
    };

    render() {


        return (
            <div className={'headerStyle'}>
                <ExpansionPanel>
                    <ExpansionPanelSummary
                        expandIcon={<PageviewIcon className={'icon-color-white'}
                                                  style={{color: '#ffffff'}}>
                        </PageviewIcon>}
                        aria-controls="panel1a-content"
                        id="panel1a-header"
                    >
                        <Typography style={{display: "none", margin: '5px', textAlign: 'center', fontSize: '20px'}}>Mercado
                            del Pilar </Typography><Avatar alt="Remy Sharp"
                                                           src={require('../../assets/img/logo/Logo-principal-trans.png')}/>
                    </ExpansionPanelSummary>
                    <ExpansionPanelDetails>
                        <Container>
                            <Row>
                                <Col sm={6} xs={12} style={{
                                    paddingTop: '10px',
                                    paddingBottom: '15px'
                                }}>
                                    <div className={'margin-10'}>
                                        <TextField
                                            id="standard-select-currency"
                                            select
                                            label="Restaurante"
                                            value={this.state.currency}
                                            onChange={this.handleChange}
                                            helperText="Selecciona"
                                        >
                                            {currencies.map(option => (
                                                <MenuItem key={option.value} value={option.value}>
                                                    {option.label}
                                                </MenuItem>
                                            ))}
                                        </TextField>
                                    </div>
                                    <div className={'margin-10'}>
                                        <TextField size="small" style={{color: 'white'}} id="outlined-basic"
                                                   label="Pedido"
                                                   variant="outlined"/>
                                    </div>
                                    <div className={'margin-10'}>
                                        <Button variant="contained" color="primary"
                                                onClick={this.example}>Consultar</Button>
                                    </div>
                                </Col>
                                {
                                    !this.state.show ?
                                        <Col sm={!this.state.show ? 6 : 12} xs={12} className={'div-center-flex'}>
                                            <Row>
                                                <Col>
                                                    <HighlightOffIcon fontSize="large"/>
                                                    <h4>NO HAY RESULTADOS</h4>
                                                </Col>

                                            </Row>
                                        </Col>
                                        :
                                        <Col sm={this.state.show ? 6 : 12} xs={this.state.show ? 12 : 0}
                                             className={`${this.state.show ? this.state.class : 'hide-detail'} div-center-flex`}
                                             style={{
                                                 paddingTop: '10px',
                                                 paddingBottom: '15px'
                                             }}>
                                            <Row>
                                                <Col style={{margin: '10px 10px'}} className={'div-center-flex'}>
                                                    <CircularProgress style={{width: '70px', height: '70px'}}
                                                                      variant="static"
                                                                      value={75}/>
                                                </Col>
                                                <Col>
                                                    <TableContainer component={Paper}>
                                                        <Table size="small" aria-label="a dense table">
                                                            <TableBody>
                                                                <TableRow key="1">
                                                                    <TableCell align="left">Estado</TableCell>
                                                                    <TableCell align="left">Preparación</TableCell>
                                                                </TableRow>
                                                                <TableRow key='2'>
                                                                    <TableCell align="left">Tiempo de
                                                                        Preparación</TableCell>
                                                                    <TableCell align="left">30 Minutos</TableCell>
                                                                </TableRow>
                                                                <TableRow key='3'>
                                                                    <TableCell align="left">Tiempo Restante</TableCell>
                                                                    <TableCell align="left">15 Minutos</TableCell>
                                                                </TableRow>
                                                            </TableBody>
                                                        </Table>
                                                    </TableContainer>
                                                </Col>
                                            </Row>
                                        </Col>
                                }


                            </Row>
                        </Container>

                    </ExpansionPanelDetails>
                </ExpansionPanel>
            </div>
        );
    }
}

export default Header;