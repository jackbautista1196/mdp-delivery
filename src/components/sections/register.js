import React, {Component} from 'react';
import {Button, Col, Form, Modal, Row} from "react-bootstrap";
import Image_Register from '../../assets/img/fondos/fondo_registro.jpg'
import {url_api_get_locations, url_api_save_restaurant} from "../Variables/Variables.jsx";
import axios from "axios";

var categorias = [

    {
        "id": 1,
        "desc_categoria": "Gourmet"
    }, {
        "id": 2,
        "desc_categoria": "De Especialidad"
    }, {
        "id": 3,
        "desc_categoria": "Buffet"
    }, {
        "id": 4,
        "desc_categoria": "De Comida Rápida"
    }, {
        "id": 5,
        "desc_categoria": "Para llevar"
    }, {
        "id": 6,
        "desc_categoria": "Fusión"
    }, {
        "id": 7,
        "desc_categoria": "Fast Casual"
    }, {
        "id": 0,
        "desc_categoria": "Otros"
    },
];

class Register extends Component {

    state = {
        name_restaurant: '',
        ruc_restaurant: '',
        reason_social: '',
        categorias_selected: 9,
        category_restaurant_other: '',
        central_restaurant: '',
        department_select: '',
        province_select: '',
        district_select: '',
        hasSucursal: '',
        direction_fiscal: '',
        name_user: '',
        name_user_contact: '',
        phone_contact: '',
        email: '',
        validated: false,
        result: false,
        res: false,
        msgRes: '',
        valueRest: 2,
        departments: [],
        getProvinces: [],
        filtersProvinces: [],
        getDistricts: [],
        filterDistricts: [],
        modalShow: false
    };

    componentDidMount = () => {
        this.getLocations();
    };

    getLocations = () => {
        axios.post(url_api_get_locations, {})
            .then(response => {
                    this.setState({
                        departments: response.data.departments,
                        getProvinces: response.data.provinces,
                        getDistricts: response.data.districts
                    })
                }
            );
    };

    saveRestaurant = (event) => {
        event.preventDefault();
        const prm = {
            name_restaurant: this.state.name_restaurant,
            ruc_restaurant: this.state.ruc_restaurant,
            reason_social: this.state.reason_social,
            categorias_selected: this.state.categorias_selected,
            category_restaurant_other: this.state.category_restaurant_other,
            central_restaurant: this.state.central_restaurant,
            department_select: this.state.department_select,
            province_select: this.state.province_select,
            district_select: this.state.district_select,
            hasSucursal: this.state.hasSucursal,
            direction_fiscal: this.state.direction_fiscal,
            name_user: this.state.name_user,
            name_user_contact: this.state.name_user_contact,
            phone_contact: this.state.phone_contact,
            email: this.state.email,
        };
        axios.post(url_api_save_restaurant, {prm})
            .then(response => {
                    this.setState({modalShow: true, res: true, msgRes: response.data.msg, valueRest: response.data.val})
                }
            );
    };

    getValueInput = key => async ({target: {value}}) => {
        console.log(value);
        switch (key) {
            case 1:
                this.setState({name_restaurant: value});
                break;
            case 2:
                this.setState({ruc_restaurant: value});
                break;
            case 3:
                this.setState({reason_social: value});
                break;
            case 4:
                this.setState({categorias_selected: value});
                break;
            case 5:
                this.setState({category_restaurant_other: value});
                break;
            case 6:
                this.setState({central_restaurant: value});
                break;
            case 7:
                this.setState({department_select: value});
                break;
            case 8:
                this.setState({province_select: value});
                break;
            case 9:
                this.setState({district_select: value});
                break;
            /*case 10:
                this.setState({email: value});
                break;*/
            case 11:
                this.setState({direction_fiscal: value});
                break;
            case 12:
                this.setState({name_user: value});
                break;
            case 13:
                this.setState({name_user_contact: value});
                break;
            case 14:
                this.setState({phone_contact: value});
                break;
            case 15:
                this.setState({email: value});
                break;
            default :
                console.log("no existe");
                break;
        }
    };

    maxLengthCheck = (object) => {
        if (object.target.value.length > object.target.maxLength) {
            object.target.value = object.target.value.slice(0, object.target.maxLength)
        }
    };

    getProvinces = async (object) => {
        console.log('[province]', object.target.value);
        const found = this.state.getProvinces.filter(province => province.idDepa === object.target.value);
        await this.setState({filtersProvinces: found, department_select: object.target.value});
        console.log(this.state);
    };

    getDistricts = async (object) => {
        console.log('[district]', object.target.value);
        const found = this.state.getDistricts.filter(district => district.idProv === object.target.value);
        await this.setState({filterDistricts: found, province_select: object.target.value});
        console.log(this.state);
    };

    getDistrictSelect = async (object) => {
        console.log('[district select]', object.target.value);
        await this.setState({district_select: object.target.value});
        console.log(this.state);
    };

    valueSetSucursal = async (object) => {
        console.log(object.target.value);
        await this.setState({hasSucursal: object.target.value});
    };

    close = () => {
        window.location.reload();
    };

    render() {
        const {categorias_selected, validated, msgRes, departments, filtersProvinces, filterDistricts, modalShow} = this.state;
        console.log(this.state);
        return (
            <div className={"content"} style={{
                backgroundImage: `url(${Image_Register})`,
                backgroundPosition: 'center center',
                backgroundRepeat: 'no-repeat',
                backgroundAttachment: 'fixed',
                height: 'auto',
                backgroundSize: 'cover',
                columnCount: 1
            }}>
                <div style={{
                    display: 'flex',
                    justifyContent: 'center',
                    alignItems: 'center',
                    height: '100%'
                }}>
                    <div className={'col-md-12'} style={{
                        display: 'flex',
                        justifyContent: 'center',
                        alignItems: 'center',
                        padding: '30px'
                    }}>
                        <div style={{
                            width: '100%', maxWidth: '50rem', padding: '15px', backgroundColor: '#ffffff'
                        }}>
                            <Row>
                                <Col md={12} lg={12} style={{textAlign: "center"}}>
                                    <h4>BIENVENIDO</h4>
                                </Col>
                            </Row>
                            <br/>
                            <Form validated={validated} onSubmit={(event) => this.saveRestaurant(event)}>
                                <Form.Group controlId="formBasicPassword">
                                    <Form.Label>Nombre de Restaurante</Form.Label>
                                    <Form.Control required type="text" onChange={this.getValueInput(1)}
                                                  minLength="1"
                                                  maxLength="160" placeholder="Ingrese nombre"/>
                                    {/*{!errorName &&*/}
                                    {/*<Form.Control.Feedback style={{display: 'flex'}}>Correcto</Form.Control.Feedback>}*/}
                                    {/*{errorName && <Form.Control.Feedback style={{display: 'flex'}}>Campo*/}
                                    {/*    Requerido</Form.Control.Feedback>}*/}
                                </Form.Group>

                                <Form.Group controlId="formBasicPassword">
                                    <Form.Label>RUC</Form.Label>
                                    <Form.Control required type="number" onInput={this.maxLengthCheck}
                                                  onChange={this.getValueInput(2)}
                                                  maxLength="11"
                                                  placeholder="Ingrese RUC"/>
                                </Form.Group>

                                <Form.Group controlId="formBasicPassword">
                                    <Form.Label>Razón Social</Form.Label>
                                    <Form.Control required type="text" onChange={this.getValueInput(3)}
                                                  minLength="1"
                                                  maxLength="160"
                                                  placeholder="Ingrese razón social"/>
                                    <Form.Control.Feedback>Correcto</Form.Control.Feedback>
                                    <Form.Control.Feedback type="invalid">Campo Requerido</Form.Control.Feedback>
                                </Form.Group>

                                <Form.Group controlId="formBasicPassword">
                                    <div>
                                        <div>
                                            <Row>
                                                <Col>
                                                    <Form.Label>Categoría de Restaurante</Form.Label>
                                                    <Form.Control as="select" required custom="true"
                                                                  onChange={(object) => this.setState({
                                                                      categorias_selected: object.target.value
                                                                  })}>
                                                        <option value=''>Seleccione categoría</option>
                                                        {
                                                            categorias &&
                                                            categorias.length > 0 &&
                                                            categorias.map(item => (
                                                                <option key={item.id}
                                                                        value={item.id}>{item.desc_categoria}</option>
                                                            ))
                                                        }
                                                    </Form.Control>
                                                </Col>
                                            </Row>
                                        </div>
                                        {
                                            categorias_selected === 0 &&
                                            <div>
                                                <br/>
                                                <Form.Group controlId="formBasicPassword">
                                                    <Form.Control type="text" onChange={this.getValueInput(5)}
                                                                  placeholder="Ingrese una categoría"/>
                                                </Form.Group>
                                            </div>
                                        }
                                    </div>
                                </Form.Group>

                                <Form.Group controlId="formBasicPassword">
                                    <Form.Label>¿Dónde se ubica la central de tu restaurante?</Form.Label>
                                    <Form.Control required type="text" onChange={this.getValueInput(6)}
                                                  placeholder="Ingrese dirección"/>
                                </Form.Group>

                                <Form.Group controlId="formBasicPassword">
                                    <Row>
                                        <Col>
                                            <Form.Label>Departamento</Form.Label>
                                            <Form.Control as="select" required custom="true"
                                                          onChange={this.getProvinces.bind(this)}>
                                                <option key={0} value=''>Seleccione Departamento</option>
                                                {
                                                    departments &&
                                                    departments.length > 0 &&
                                                    departments.map(item => (
                                                        <option key={item.idDepa} value={item.idDepa}>{item.departamento}</option>
                                                    ))
                                                }
                                            </Form.Control>
                                        </Col>
                                        <Col>
                                            <Form.Label>Provincia</Form.Label>
                                            <Form.Control as="select" required custom="true"
                                                          onChange={this.getDistricts.bind(this)}>
                                                <option value=''>Seleccione Provincia</option>
                                                {
                                                    filtersProvinces &&
                                                    filtersProvinces.length > 0 &&
                                                    filtersProvinces.map(item => (
                                                        <option key={item.idProv} value={item.idProv}>{item.provincia}</option>
                                                    ))
                                                }
                                            </Form.Control>
                                        </Col>
                                        <Col>
                                            <Form.Label>Distrito</Form.Label>
                                            <Form.Control as="select" required custom="true"
                                                          onChange={this.getDistrictSelect.bind(this)}>
                                                <option value=''>Seleccione Distrito</option>
                                                {
                                                    filterDistricts &&
                                                    filterDistricts.length > 0 &&
                                                    filterDistricts.map(item => (
                                                        <option key={item.idDist} value={item.idDist}>{item.distrito}</option>
                                                    ))
                                                }
                                            </Form.Control>
                                        </Col>
                                    </Row>
                                </Form.Group>

                                <Form.Group controlId="formBasicPassword">
                                    <Form.Label>Dirección Fiscal</Form.Label>
                                    <Form.Control required type="text" onChange={this.getValueInput(11)}
                                                  minLength="1"
                                                  maxLength="160"
                                                  placeholder="Ingrese dirección"/>
                                </Form.Group>

                                <Form.Group controlId="formBasicPassword">
                                    <Form.Label>¿Tiene otras sucursales que realizarán delivery?</Form.Label>
                                    <Form.Check
                                        onChange={this.valueSetSucursal.bind(this)}
                                        required
                                        type={'radio'}
                                        name={"prob"}
                                        label={'Si'}
                                        id={"radio1"}
                                        value={1}
                                    />
                                    <Form.Check
                                        required
                                        onChange={this.valueSetSucursal.bind(this)}
                                        type={'radio'}
                                        name={"prob"}
                                        label={'No'}
                                        id={"radio2"}
                                        value={2}
                                    />
                                    <Form.Control.Feedback>Correcto</Form.Control.Feedback>
                                    <Form.Control.Feedback type="invalid">Campo Requerido</Form.Control.Feedback>
                                </Form.Group>

                                <Form.Group controlId="formBasicPassword">
                                    <Form.Label>Nombre y Apellido de Representante Legal</Form.Label>
                                    <Form.Control required type="text" onChange={this.getValueInput(12)}
                                                  minLength="1"
                                                  maxLength="160"
                                                  placeholder="Ingrese dato"/>
                                    <Form.Control.Feedback>Correcto</Form.Control.Feedback>
                                    <Form.Control.Feedback type="invalid">Campo Requerido</Form.Control.Feedback>
                                </Form.Group>

                                <Form.Group controlId="formBasicPassword">
                                    <Form.Label>Nombre y Apellido de contacto</Form.Label>
                                    <Form.Control required type="text" onChange={this.getValueInput(13)}
                                                  minLength="1"
                                                  maxLength="160"
                                                  placeholder="Ingrese dato"/>
                                    <Form.Control.Feedback>Correcto</Form.Control.Feedback>
                                    <Form.Control.Feedback type="invalid">Campo Requerido</Form.Control.Feedback>
                                </Form.Group>

                                <Form.Group controlId="formBasicPassword">
                                    <Form.Label>Teléfono de contacto</Form.Label>
                                    <Form.Control required type="number" onChange={this.getValueInput(14)}
                                                  onInput={this.maxLengthCheck}
                                                  maxLength="10"
                                                  placeholder="Ingrese telefono"/>
                                    <Form.Control.Feedback>Correcto</Form.Control.Feedback>
                                    <Form.Control.Feedback type="invalid">Campo Requerido</Form.Control.Feedback>
                                </Form.Group>

                                <Form.Group controlId="formBasicEmail">
                                    <Form.Label>Email</Form.Label>
                                    <Form.Control required type="email" onChange={this.getValueInput(15)}
                                                  placeholder="Ingrese email"/>
                                    <Form.Control.Feedback>Correcto</Form.Control.Feedback>
                                    <Form.Control.Feedback type="invalid">Campo Requerido</Form.Control.Feedback>
                                </Form.Group>

                                <Button variant="primary" type="submit">
                                    Registrar
                                </Button>
                            </Form>
                            <Modal show={modalShow}
                                   size="lg"
                                   aria-labelledby="contained-modal-title-vcenter"
                                   centered>
                                <Modal.Header closeButton>
                                    <Modal.Title id="contained-modal-title-vcenter">
                                        Respuesta de Altoque.Rest
                                    </Modal.Title>
                                </Modal.Header>
                                <Modal.Body>
                                    <p>
                                        {msgRes}
                                    </p>
                                </Modal.Body>
                                <Modal.Footer>
                                    <Button onClick={() => this.close()}>Cerrar</Button>
                                </Modal.Footer>
                            </Modal>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Register;