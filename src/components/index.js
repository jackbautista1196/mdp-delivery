export {default as Principal} from './principal/index';
export {default as Header} from './sections/header';
export {default as About} from './sections/about';
export {default as View} from './sections/view';
export {default as Register} from './sections/register';